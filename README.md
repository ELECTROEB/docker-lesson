These are some docker examples to be used in workshop form.

<H2>Setup</H2>

1. go to [docker.com](https://www.docker.com/) and create an account if you don't already have one.
2. go to [labs.play-with-docker.com](https://labs.play-with-docker.com/) and click __Login__ and/or __Start__.
3. On the sidebar on the right, click __+ADD NEW INSTANCE__
4. In the terminal write/paste


   `git clone https://gitlab.com/devops-university-course22/docker-lesson.git`


<H2>A. Docker first steps</H2>

5. Enter the docker-lesson folder then the __A__ folder

6. run `docker build .`
7. run `docker run <image_id>` (the image_id was printed in the last line of the build step)
8. Notice the printed text
9. You can modify the text in main.py, repeat steps 6-7 and the change will be reflected there.


<H2>B. Use a compose file</H2>

10.  Go back to the docker-lesson folder then the __B__ folder
11.  run  `docker build .`
12.  run `docker run -p 5000:80 <image_id>`
13.  click on the __OPEN PORT__ button on the top of the page.
14.  In the open dialog write `5000`
15.  In the tab opened notice the printed text
16.  stop the container by doing _CTRL + C_ from your keyboard.
17.  run `docker compose up`
    
18.  On top of the page there is already a button with the `5000` port, click on it so that the webpage opens again.
19. Notice that we have the same result but it's easier because the additional parameters are already included in the compose file
20. You can modify the text in index.html, repeat steps 11-14 and the change will __not__ be reflected there. In order for it to be reflected compose needs to be told to not use the cached image. Try and find how to do that.
   
   <H2>C. A bit more complicated dockerfile</H2>

21. Go back to the docker-lesson folder then the __C__ folder

22. run `docker compose up`
23. On top of the page there is already a button with the `5000` port, click on it so that the webpage opens again. Or repeat steps 13-14